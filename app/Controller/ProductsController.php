<?php
class ProductsController extends AppController {
    
    public $uses = array("Category", "CategoryLang", "Product");
    
    public function index(){
        $this->set("categories", $this->Category->getCategories());
        $this->set("products", $this->Product->getProductsByCategoryId());
    }
    
    public function getCategoryProducts(){
        $this->layout = false;
        $this->render(false);
        header("Content-Type: text/plain; charset=utf-8");
        
        $limit = $this->request->data("limit");
        $id_category = $this->request->data("id_category");
        $page = $this->request->data("page");
        $order = $this->request->data("order");
        $col = $this->request->data("col");
        
        $limit = (empty($limit)) ? 30 : $limit;
        $page = (empty($page)) ? 1 : $page;
        
        if($id_category > 0){
            $count = count($this->Product->getProductsByCategoryId($id_category, null));
            $pages = ceil($count/$limit);
            $offset = $page * $limit - $limit;
            
            $products = $this->Product->getProductsByCategoryId($id_category, $limit, $offset,$order, $col);
            echo "{";
            echo "pages : {$pages}, page : {$page}, count : {$count},";
            echo "products : [";
            $i = 0;
            foreach ($products as $product){
                echo "{";
                echo "id_product : \"{$product["p"]["id_product"]}\"
                     ,name : \"{$product["pl"]["product_name"]}\"
                     ,ean13 : \"{$product["p"]["ean13"]}\"
                     ,position : \"{$product["cp"]["position"]}\"";
                echo "}";
                $i++;
                if($i < $limit){
                    echo ",";
                }
            }
            echo "]";
            echo "}";
            
        }else{
            echo "{";
            echo "status : \"faild\", message : \"Brak ID kategorii!\"";
            echo "}";
        }
    }
    
    public function updatePosition(){
        $this->layout = false;
        $this->render(false);
        header("Content-Type: text/plain; charset=utf-8");
        
        $id = $this->request->data("id_product");
        $position = $this->request->data("position");
        $category = $this->request->data("category");
        
        $this->Product->query("UPDATE ps_category_product SET position=".(int)$position." WHERE id_product=".(int)$id." AND id_category=".(int)$category);
    }
}
?>