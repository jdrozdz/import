<?php 
set_time_limit(30);

class SyncController extends AppController {
	public $uses = array('Import','CakeNumber', 'Utility','Product','Stock','Attribute','ImportProduct','Category','FeatureProduct','FeatureValueLang');
	
	private $_uploadData = "webroot/files/sync";
	
	public function beforeFilter(){
		if(!$this->Session->check('Auth')){
			$this->redirect(array('controller' => 'users','action' => 'login'));
		}
	}
	
	public function index(){
		$this->set('files', $this->Import->find('all', array('conditions' => array('Import.type' => 'snc'))));
	}
	
	public function withoutPics(){
		@$upc = $this->request->query['upc'];
		$this->set('upc', ( ($upc) ? $upc : 0 ) ) ;
		$this->set('items', $this->Product->getProductsWithoutPics(( ($upc) ? $upc : 0 )));
	}
	
	public function sendFile(){
		if($this->request->is('post')){
			if(empty($this->request->data['Document']['datafile']['error']) 
				|| $this->request->data['Document']['datafile']['error'] == 0){
				
				$size = $this->request->data['Document']['datafile']['size'];
				$ext = pathinfo($this->request->data['Document']['datafile']['name'], PATHINFO_EXTENSION);
				$name =substr($this->request->data['Document']['datafile']['name'],0,( (strlen($ext)+1)*(-1) ))."-".date("Ymd");
				if(file_exists('files/sync/'.$name.".".$ext)){
					$name = $name."(".date("H_i_s").")";
				}
				$mv = move_uploaded_file($this->request->data['Document']['datafile']['tmp_name'], "files/sync/".$name.".".$ext);
				if(
				$mv
				){
					$this->Import->create();
					$this->Import->save(
						array(
							'filename' => $name,
							'ext' => $ext,
							'filesize' => $size,
							'created_date' => date('Y-m-d'),
							'type'	=> 'snc'
						)
					);
					$this->Session->setFlash('Plik został dodany');
					$this->redirect(array('action' => 'index'));
				}else{
					$this->Session->setFlash('Błąd przesunięcia pliku!');
				}
			}
		}
	}
	
	public function synchronization($id){
		$row = $this->Import->findById($id);
		
		$data = file_get_contents(dirname(__FILE__).'/../webroot/files/sync/'.$row['Import']['filename'].'.'.$row['Import']['ext']);
		
		$this->set('filename', $row['Import']['filename']);
		$this->set('data', $data);
	}
	
	public function deleteFile($id = null){
		if(empty($id)){
			throw new MethodNotAllowedException();
		}
		
		$file = $this->Import->findById($id);
		$fpath = APP."/".$this->_uploadData."/".$file['Import']['filename'].".".$file['Import']['ext'];
		if(unlink($fpath)){
			if($this->Import->delete($id)){
				$this->Session->setFlash("Plik został usunięty");
			}else{
				$this->Session->setFlash('Plik unusięta lecz nadal jest zapisany w bazie!<br />Proszę skontaktować się z administratorem.');
			}
		}else{
			$this->Session->setFlash("Błąd usunięcia lub nie ma takiego pliku");
		}
		
		$this->redirect(array('action' => 'index'));
	}
	
	public function lockFile($id = null){
		if(empty($id)){
			throw new MethodNotAllowedException();
		}
		$file = $this->Import->findById($id);
		$this->Import->id = $id;
		$this->Import->save(array(
			'flag' => ($file['Import']['flag'] == 'ENABLE') ? 'DISABLE' : 'ENABLE'
		));
		
		$this->Session->setFlash("Plik <tt>{$file['Import']['filename']}</tt> został zablokowany");
		$this->redirect(array('action' => 'index'));
	}
	
	public function makeSync(){
		$this->layout = false;
		$this->render(false);
		
		if($this->request->is('get')){
			echo "GET<br />";
			$name = $this->request->query['name'];
			$col = $this->request->query['isbn'];
			$quantity = $this->request->query['quantity'];
		}else{
			$name = $this->request->data['name'];
			$col = $this->request->data['isbn'];
			$quantity = $this->request->data['quantity'];
		}
		$product = $this->Product->find('first',
			array('conditions' => 
				array('Product.name' => trim($name) )
			)
		);

		if(count($product) > 0){
			$isbn = $col;//substr($col,0,3)."-".substr($col, 3,2)."-".substr($col, 5,4)."-".substr($col, 9,3)."-".substr($col, 12,1);
			$id_product = $product['Product']['id_product'];
			
			if(count($product) && $col != 'delete'){
				$value = $this->FeatureProduct->find('first', array('conditions' => 
					array( 'FeatureProduct.id_product' => $id_product,
						'FeatureProduct.id_feature' => 1
					)
					));
				if(count($value) > 0){	
					$this->FeatureValueLang->updateAll(
						array('FeatureValueLang.value' => (string)$isbn),
						array('FeatureValueLang.id_feature_value' => $value['FeatureProduct']['id_feature_value'] )
					);
				}
				echo "OK"; //$id_product." | ".$value['FeatureProduct']['id_feature_value']." | ".$isbn;
			}else{
				// kosz 28
				$cat = $this->Category->find('first', array('conditions' => array('Category.id_product' => $id_product)));
				if(count($cat) > 0){
					$this->Category->updateAll(
						array('Category.id_category' => 28),
						array('Category.id_product' => $id_product, 'Category.id_category' => $cat['Category']['id_category'])
					);
				}else{
					$this->Category->create();
					$this->Category->save(array(
						"id_category" => 28,
						"id_product" => $id_product,
						"position" => 0
					));
				}
				
				$this->Product->id_product = $id_product;
				$this->Product->save(array( 'id_category_default' => 28 ));
				
				echo "Moved to Trash";
			}
		}else{
			echo 'FAILD';
		}
	}
}
?>