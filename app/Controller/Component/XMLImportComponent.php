<?php
class XMLImportComponent extends Component {
	private $Import;
	private $FeatureValueLang;
	private $FeatureValue;
	private $FeatureProduct;
	private $SpecificPrice;
	
	public function __construct(){
		$this->Import = ClassRegistry::init('Import');
		$this->FeatureValue = ClassRegistry::init('FeatureValue');
		$this->FeatureValueLang = ClassRegistry::init('FeatureValueLang');
		$this->FeatureProduct = ClassRegistry::init('FeatureProduct');
		$this->SpecificPrice = ClassRegistry::init('SpecificPrice');
	}
	
	public function importFeatures(SimpleXMLElement $xml){
//		die(pr($xml));
		switch($xml->info->scope['name']){
			case 'features':
			$k = 1;
			foreach($xml->rows->feature as $row){
				$_id_product = $row['id_product'];
				$_id_feature = $row['id_feature'];
				$_id_feature_value = $k; //$row['id_feature_value'];
				$_custom = $row['custom'];
				$_value = $row['value'];
				$this->_addFeatureValue($_id_feature_value,$_id_feature);
				$this->_addFeatureProduct($_id_feature, $_id_product, $_id_feature_value);
				$v = ( $_value == "Enter custom value" ) ? (string)$_custom : (string)$_value;
				$this->_addFeatureLang($_id_feature_value, $v);
				$k++;
			}
			break;
			default:
				throw new Exception("Błąd przestrzeni nazw!");
		}
	}
	// Features
	private function _addFeatureLang($_id_feature_value, $val){
		$langs[0] = 1;
		$langs[1] = 7;
		
		for($i =0; $i < 2; $i++){
			$row = $this->FeatureValueLang->find('count', array('conditions' => array(
				'FeatureValueLang.id_feature_value' => $_id_feature_value, 
				'FeatureValueLang.id_lang' => $langs[$i],
				'FeatureValueLang.value' => $val
			)));
			if($row == 0){
				$this->FeatureValueLang->create();
				$this->FeatureValueLang->save(
					array(
						'id_feature_value' => $_id_feature_value,
						'id_lang' => $langs[$i],
						'value' => $val
					)
				);
			}
		}
	}
	private function _addFeatureValue($_id_feature_value, $_id_feature){
		$row = $this->FeatureValue->find('count', array('conditions' => array(
			'FeatureValue.id_feature_value' => $_id_feature_value, 
			'FeatureValue.id_feature' => $_id_feature
		)));
		
		if($row == 0){
			$this->FeatureValue->create();
			$this->FeatureValue->save(array(
				'id_feature_value' => $_id_feature_value,
				'id_feature' => $_id_feature,
				'custom' => 1
			));
		}
	}
	private function _addFeatureProduct($_id_feature, $_id_product, $_id_feature_value){
		$row = $this->FeatureProduct->find('count', array('conditions' => array(
			'FeatureProduct.id_product' => $_id_product,
			'FeatureProduct.id_feature' => $_id_feature,
			// 'FeatureProduct.id_feature_value' => $_id_feature_value
		)));
		if($row == 0){
			$this->FeatureProduct->create();
			$this->FeatureProduct->save(array(
				'id_feature' => $_id_feature,
				'id_product' => $_id_product,
				'id_feature_value' => $_id_feature_value
			));
		}
	}
	// ----****--------------
}
?>