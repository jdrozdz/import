<?php 
class AttributesController extends AppController {
	public $uses = array('Import','CakeNumber', 'Utility','Product','ProductModel','Stock','Attribute','ImportProduct','SpecificPrice');
	public $components = array('XMLImport');
	
	private $_uploadData = "webroot/files/xml";
	
	public function beforeFilter(){
		if(!$this->Session->check('Auth')){
			$this->redirect(array('controller' => 'users','action' => 'login'));
		}
	}
	public function index(){
		$this->set('files', $this->Import->find('all', array('conditions' => array('Import.type' => 'xml'))));
	}
	
	public function sendFile(){
		if($this->request->is('post')){
			if(empty($this->request->data['Document']['datafile']['error']) 
				|| $this->request->data['Document']['datafile']['error'] == 0){
				
				$size = $this->request->data['Document']['datafile']['size'];
				$ext = pathinfo($this->request->data['Document']['datafile']['name'], PATHINFO_EXTENSION);
				$name =substr($this->request->data['Document']['datafile']['name'],0,( (strlen($ext)+1)*(-1) ))."-".date("Ymd");
				if(file_exists('files/xml/'.$name.".".$ext)){
					$name = $name."(".date("H_i_s").")";
				}
				$mv = move_uploaded_file($this->request->data['Document']['datafile']['tmp_name'], "files/xml/".$name.".".$ext);
				if(
				$mv
				){
					$this->Import->create();
					$this->Import->save(
						array(
							'filename' => $name,
							'ext' => $ext,
							'filesize' => $size,
							'created_date' => date('Y-m-d'),
							'type' => 'xml'
						)
					);
					$this->Session->setFlash('Plik został dodany');
					$this->redirect(array('action' => 'index'));
				}else{
					$this->Session->setFlash('Błąd przesunięcia pliku!');
				}
			}
		}
	}
	
	public function importData($id){
		$row = $this->Import->findById($id);
		$xmlPath = "files/xml/".$row['Import']['filename'].".".$row['Import']['ext'];
		$xml = simplexml_load_file($xmlPath);
		$this->set('xml', $xml);
	}
	public function importAjax(){
		$this->layout = false;
		$this->render(false);
		
		$id_product = $this->request->data('id_product');
		$discount = $this->request->data('discount');
		
		$row = $this->SpecificPrice->find('count', array('conditions' => array('SpecificPrice.id_product' => $id_product)));
		$data = array(
				'id_product' => $id_product,
				'price' => -1,
				'from_quantity' => 1,
				'reduction' => $discount
			);
		if($row){
			$this->SpecificPrice->id_specific_price_available = $row['SpecificPrice']['id_specific_price_available'];
		}
		$this->SpecificPrice->save($data);
		if(count($this->SpecificPrice->read()) > 0){
			echo "OK";
		}else{
			echo "FAILD";
		}
	}
	
	public function deleteFile($id = null){
		if(empty($id)){
			throw new MethodNotAllowedException();
		}
		
		$file = $this->Import->findById($id);
		$fpath = APP."/".$this->_uploadData."/".$file['Import']['filename'].".".$file['Import']['ext'];
		if(unlink($fpath)){
			if($this->Import->delete($id)){
				$this->Session->setFlash("Plik został usunięty");
			}else{
				$this->Session->setFlash('Plik unusięta lecz nadal jest zapisany w bazie!<br />Proszę skontaktować się z administratorem.');
			}
		}else{
			$this->Session->setFlash("Błąd usunięcia lub nie ma takiego pliku");
		}
		
		$this->redirect(array('action' => 'index'));
	}
	
	public function lockFile($id = null){
		if(empty($id)){
			throw new MethodNotAllowedException();
		}
		$file = $this->Import->findById($id);
		$this->Import->id = $id;
		$this->Import->save(array(
			'flag' => ($file['Import']['flag'] == 'ENABLE') ? 'DISABLE' : 'ENABLE'
		));
		
		$this->Session->setFlash("Plik <tt>{$file['Import']['filename']}</tt> został zablokowany");
		$this->redirect(array('action' => 'index'));
	}
	
	public function importFile($id = null){
		$rdr_scope = array("product");
		$this->render(false);
		if(empty($id)){
			$this->Session->setFlash("Błędne ID");
			$this->redirect(array('action'=>'index'));
		}
		$row = $this->Import->findById($id);
		$xmlPath = "files/xml/".$row['Import']['filename'].".".$row['Import']['ext'];
		$xml = simplexml_load_file($xmlPath);
		if(in_array($xml->info->scope['name'], $rdr_scope)){
			$this->redirect(array('action' => 'importData', $id));
		}
		$this->XMLImport->importFeatures($xml);
		$this->Session->setFlash("Import zakończony");
		$this->redirect(array('action' => 'index'));
	}
	
	public function updateIndexes(){
		$this->set('products', $this->Product->loadProductsWithoutIndex());
	}
	
	public function updateIndexAjax(){
		$this->layout = false;
		$this->render(false);
		
		if($this->request->is('get')){
			echo "GET<br />";
			$id_product = $this->request->query['id_product'];
			$ean13 = $this->request->query['ean13'];
			$reference = $this->request->query['reference'];
		}else{
			$id_product = $this->request->data['id_product'];
			$ean13 = $this->request->data['ean13'];
			$reference = $this->request->data['reference'];
		}
		
		$data = array(
			'reference' => $reference,
			'ean13' => $ean13
		);
		$product = $this->ProductModel->find('first', array( 'conditions' => array( 'id_product' => (int)$id_product ) ));
		if(count($product) == 1){
			$update = $this->ProductModel->updateAll(
				array('ProductModel.reference' => $reference, 'ProductModel.ean13' => $ean13),
				array('ProductModel.id_product' => $id_product)
			);
			if($update){
				echo "OK";
			}else{
				echo 'FAILD';
			}
		}
	}
}
?>