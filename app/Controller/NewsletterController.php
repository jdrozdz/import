<?php
class NewsletterCOntroller extends AppController {
    public $uses = array('Newsletter');
    
    public function beforeFilter(){
		//$this->Auth->allow(array('form','done','dane'));
    }
    
    public function index(){
    	$this->render(false);
    }
    
	public function form(){
            $this->layout = 'form';
            if($this->request->is('post')){
                $this->Newsletter->create();

                $name = $this->request->data['customer']['name'];
                $lastname = $this->request->data['customer']['lastname'];
                $address = $this->request->data['customer']['address'];
                $zipcode = $this->request->data['customer']['zipcode'];
                $city = $this->request->data['customer']['city'];
                $email = $this->request->data['customer']['email'];
                $this->Newsletter->save(array(
                    'name' => $name,
                    'lastname' => $lastname,
                    'address'   => $address,
                    'zipcode'   => $zipcode,
                    'city'      => $city,
                    'email'     => $email,
                    'date_added' => date('Y-m-d')
                ));
                //$this->Session->setFlash("Dane zostały zapisane, dziękujemy!");
                $this->redirect(array('action'=>'done'));
            }
	}
        
        public function done(){
            $this->layout = 'form';
        }
        
        public function dane(){
	        if(!$this->Session->check('Auth')){
				$this->redirect(array('controller' => 'users','action' => 'login'));
			}
            $this->set("customers", $this->Newsletter->find('all'));
        }
}
?>