<?php
class IndexController extends AppController{
	public $uses = array('Import','CakeNumber', 'Utility','Product','Stock','Attribute','ImportProduct', 'InvertoryData', 'StockModel');
	
	private $_uploadData = "webroot/files/data";
	
	public function beforeFilter(){
		if(!$this->Session->check('Auth')){
			$this->redirect(array('controller' => 'users','action' => 'login'));
		}
	}
	
	public function dashboard(){}
	
	public function index(){
		$this->set("InvertoryData", $this->InvertoryData);
		$this->set('files', $this->Import->find('all', array('conditions' => array('Import.type' => 'csv'))));
	}
	
	public function sendFile(){
		if($this->request->is('post')){
			if(empty($this->request->data['Document']['datafile']['error']) 
				|| $this->request->data['Document']['datafile']['error'] == 0){
				
				$size = $this->request->data['Document']['datafile']['size'];
				$ext = pathinfo($this->request->data['Document']['datafile']['name'], PATHINFO_EXTENSION);
				$name =substr($this->request->data['Document']['datafile']['name'],0,( (strlen($ext)+1)*(-1) ))."-".date("Ymd");
				if(file_exists('files/data/'.$name.".".$ext)){
					$name = $name."(".date("H_i_s").")";
				}
				$mv = move_uploaded_file($this->request->data['Document']['datafile']['tmp_name'], "files/data/".$name.".".$ext);
				if(
				$mv
				){
					$this->Import->create();
					$this->Import->save(
						array(
							'filename' => $name,
							'ext' => $ext,
							'filesize' => $size,
							'created_date' => date('Y-m-d'),
							'type'	=> 'csv'
						)
					);
					$this->Session->setFlash('Plik został dodany');
					$this->redirect(array('action' => 'index'));
				}else{
					$this->Session->setFlash('Błąd przesunięcia pliku!');
				}
			}
		}
	}
	
	public function deleteFile($id = null){
		if(empty($id)){
			throw new MethodNotAllowedException();
		}
		
		$file = $this->Import->findById($id);
		$fpath = APP."/".$this->_uploadData."/".$file['Import']['filename'].".".$file['Import']['ext'];
		if(unlink($fpath)){
			if($this->Import->delete($id)){
				$this->Session->setFlash("Plik został usunięty");
			}else{
				$this->Session->setFlash('Plik unusięta lecz nadal jest zapisany w bazie!<br />Proszę skontaktować się z administratorem.');
			}
		}else{
			$this->Session->setFlash("Błąd usunięcia lub nie ma takiego pliku");
		}
		
		$this->redirect(array('action' => 'index'));
	}
	
	public function lockFile($id = null){
		if(empty($id)){
			throw new MethodNotAllowedException();
		}
		$file = $this->Import->findById($id);
		$this->Import->id = $id;
		$this->Import->save(array(
			'flag' => ($file['Import']['flag'] == 'ENABLE') ? 'DISABLE' : 'ENABLE'
		));
		
		$this->Session->setFlash("Plik <tt>{$file['Import']['filename']}</tt> został zablokowany");
		$this->redirect(array('action' => 'index'));
	}
	
	public function importFile($id = null){
		if(empty($id)){
			throw new MethodNotAllowedException();
		}
		
		$data = $this->Import->findById($id);
		$data_file = APP.$this->_uploadData."/".$data['Import']['filename'].".".$data['Import']['ext'];
		
		$invertory_doc = $this->InvertoryData->findById($data['Import']['id_invertory_doc']);
		
		@$this->set('inv_doc_no', $invertory_doc['InvertoryData']['document_no']);
		@$this->set('inv_doc_date', $invertory_doc['InvertoryData']['document_date']);
		
		
		$this->set('data',file_get_contents($data_file));
		$this->set('importProductMod', $this->ImportProduct);
		$this->set("id", $id);
	}
	
	public function importAjax(){
		$id_product = $this->request->data('id_product');
		$quantity = $this->request->data('quantity');
		$doc_no = $this->request->data('id_document');
		$reference = $this->request->data('reference');
		$ean = $this->request->data('ean');
		$wphq = $this->request->data('was_quantity');
		$price = $this->request->data('hurt');
		
		$this->layout = false;
		$this->render(false);
		
		$product = $this->ImportProduct->find('first',array(
			'conditions' => array(
				'ImportProduct.id_product' => $id_product
			)));	
		if(count($product)){
			$this->StockModel->create();
			$this->StockModel->save(array(
				'id_warehouse' => 1,
				'id_product'	=> $id_product,
				'reference'		=> $reference,
				'ean13'			=> $ean,
				'physical_quantity'	=> $quantity,
				'usable_quantity'	=> $quantity,
				'price_te'			=> $price,
				'id_invertory_document'	=> $doc_no
			));
			$this->_updateStock($id_product, $quantity);
			echo 'OK';
		}else{
			echo "FAILD";
		}
	}
	
	private function _updateStock($id_product, $quantity){
		$this->Stock->updateAll(
				// Fields to update
				array('Stock.quantity' => $quantity, 'Stock.depends_on_stock' => 1),
				// Conditions
				array('Stock.id_product' => $id_product)
			);
	}
	
	public function addInvertoryDocument(){
		$this->layout = false;
		$this->render(false);
		
		$no = $this->request->data('invoice_number');
		$date = $this->request->data('document_date');
		$add_date = date('Y-m-d');
		
		$document = $this->InvertoryData->find('first',array(
			'conditions' => array(
				'document_no' => $no
			)
		));
		
		if(count($document)){
			echo $document['InvertoryData']['id'];
		}else{
			
			// aktualizacje do dokumentow
			/* TUTAJ */
			//------
			$doc = $this->InvertoryData->create();
			$this->InvertoryData->save(array(
				'document_no' 	=> $no,
				'document_date' => $date,
				'add_date'		=> $add_date
			));
			
			echo $this->InvertoryData->id;
		}
		
	}
	
	public function updateStockDock(){
		$this->layout = false;
		$this->render(false);
		
		$id = $this->request->data('doc_no');
		$id_file = $this->request->data('id_file');
		
		$this->Import->updateAll(
			array('Import.id_invertory_doc' => $id),
			array('Import.id' => $id_file)
		);
	}
	
	public function snap(){
		$this->layout = 'form';
	}
}
?>