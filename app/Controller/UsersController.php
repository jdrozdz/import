<?php 
class UsersController extends AppController{
	
	public function beforeFilter(){
		
	}
	
	public function index(){
		$this->render(false);
	}
	
	public function login(){
		if($this->request->is('post') && !empty($this->data)){
			$username = $this->data['User']['username'];
			$password = sha1($this->data['User']['password']);
			
			$user = $this->User->find('all',array(
				'conditions' => array(
					'User.username' => $username,
					'User.password' => $password,
					'User.enabled'	=> 1
				)
			));
			
			if(count($user) === 1){
				$this->Session->write('Auth', $user);
				$this->redirect(array('controller' => 'index', 'action' => 'dashboard'));
			}else{
				$this->User->invalidate('username','Login lub hasło jest niepoprawne!');
				$this->User->invalidate('password','Login lub hasło jest niepoprawne!');
			}
		}
	}
	
	public function logout(){
		$this->Session->delete('Auth');
		$this->Session->setFlash("Zostałeś wylogowany");
		$this->redirect(array('action' => 'login'));
	}
	
	public function register(){
		if($this->request->is('post') && !empty($this->data)){
			if($this->User->validates()){
				$data['username'] = $this->data['User']['username'];
				$data['password'] = sha1($this->data['User']['password']);
				$data['name'] = $this->data['User']['name'];
				$data['lastname'] = $this->data['User']['lastname'];
				$data['email'] = $this->data['User']['email'];
				
				$this->User->save($data);
//				$sql_data = $this->User->read();
//				$this->Auth->login($sql_data);
//				$aro = new Aro();   
//				$parent = $aro->findByAlias($this->User->find('count') > 1 ? 'user' : 'root');
//				$aro->create();   
//				$aro->save(array(   
//				     'model'        => 'User',   
//				     'foreign_key'    => $this->User->id,   
//				     'parent_id'    => $parent['Aro']['id'],   
//				     'alias'        => 'User::'.$this->User->id   
//				));
				$this->Session->setFlash("Rejestracja przebiegła poprawnie");
				$this->redirect("/");
			}
		}
	}
	
	public function install(){
		$this->render(false);
	}
	
}
?>