create table import_data (
id int unsigned auto_increment primary key,
filename varchar(100) not null unique,
filesize int not null,
created_date timestamp not null default CURRENT_TIMESTAMP
)ENGINE=InnoDB DEFAULT CHARSET=utf8;
create table newsletter (
id int unsigned auto_increment primary key,
name varchar(80) not null,
lastname varchar(80) not null,
address varchar(15) not null,
zipcode varchar(15) not null,
city varchar(30) not null
)ENGINE=InnoDB DEFAULT CHARSET=utf8;
create view import_product as
select p.id_product, p.name, fvl.value as attr_value from ps_product_lang as p
join ps_feature_product as fp on fp.id_product = p.id_product
join ps_feature_value_lang as fvl on fvl.id_feature_value = fp.id_feature_value
where p.id_lang = 1