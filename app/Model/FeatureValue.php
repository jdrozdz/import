<?php 
class FeatureValue extends AppModel {
	public $useTable = "feature_value";
	public $tablePrefix = "ps_";
	
	public $hasMany = array(
		'FeatureValueLang' => array(
			'className' => 'FeatureValueLang',
		)
	);
}
?>