<?php
class Product extends AppModel {
	public $useTable = 'product_lang';
	public $tablePrefix = "ps_";
	
	public $hasMany = array(
		'FeatureProduct' => array(
			'className' => 'FeatureProduct'
		)
	);
	
	public function getProductsWithoutPics( $upc = true ){
		$products = $this->query("SELECT fvl.value as isbn, p.id_product, pl.name as title
			FROM ps_product as p 
			JOIN ps_product_lang as pl ON p.id_product=pl.id_product 
			join ps_feature_product as fp on fp.id_product=p.id_product 
			join ps_feature_value_lang as fvl on fvl.id_feature_value=fp.id_feature_value
			WHERE fp.id_feature=1 and pl.id_lang = 7".( ($upc) ? " and p.upc = 1" : " and p.upc = '' ")." group by fvl.value order by p.id_product ASC");
		return $products;
	}
	
	public function loadProductsWithoutIndex(){
		$sql = "SELECT replace(fvl.value,'-','') AS isbn, p.id_product, pl.name AS title 
				FROM ps_product AS p 
				JOIN ps_product_lang AS pl ON p.id_product = pl.id_product 
				JOIN ps_feature_product AS fp ON fp.id_product = p.id_product 
				JOIN ps_feature_value_lang AS fvl ON fvl.id_feature_value = fp.id_feature_value 
				WHERE fp.id_feature =1 
				AND p.reference = '' 
				AND pl.id_lang = 7
				GROUP BY isbn
				ORDER BY p.id_product ASC";
		return $this->query($sql);
	}

	public function disableProductsFromShop(){
		$this->query("
		update ps_product as p,ps_category_product as cp, ps_product_shop as ps 
		set p.active=0, p.visibility='none',p.redirect_type=404, ps.active=0, ps.visibility='none',ps.redirect_type=404
		where p.id_product = cp.id_product and cp.id_category=29 and ps.id_product = p.id_product
		");
	}
        
        public function getProductsByCategoryId($id_category = null, $limit = 30, $offset = 0, $order = "ASC", $col = "p.id_product"){
            $sql = "SELECT DISTINCT p.id_product, pl.name AS product_name, cl.name AS category_name, p.ean13, cp.position FROM ps_product AS p ";
            $sql .= "JOIN ps_product_lang AS pl ON pl.id_product = p.id_product ";
            $sql .= "JOIN ps_category_product AS cp ON p.id_product = cp.id_product ";
            $sql .= "JOIN ps_category AS c ON c.id_category = cp.id_category ";
            $sql .= "JOIN ps_category_lang AS cl ON c.id_category = cl.id_category ";
            $sql .= ($id_category != null) ? "WHERE c.id_category = ".(int)$id_category." AND pl.id_lang=7 AND cl.id_lang=7 " : " WHERE pl.id_lang=7  AND cl.id_lang=7 ";
            $sql .= " ORDER BY {$col} {$order}";
            $sql .= ($limit == null) ? "" : " LIMIT {$offset}, {$limit}";
            return $this->query($sql);
        }
}
?>