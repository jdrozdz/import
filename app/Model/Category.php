<?php
// KOSZ - 28
class Category extends AppModel {
	public $useTable = "category_product";
        public $tablePrefix = "ps_";
        
        public function getCategories(){
            $sql = "SELECT DISTINCT c.id_category, cl.name FROM ps_category AS c ";
            $sql .= "JOIN ps_category_lang AS cl ON c.id_category = cl.id_category";
            $sql .= " WHERE cl.id_lang = 7";
            
            return $this->query($sql);
        }
}
?>