<h2>Aktualizacja indeksów oraz EAN13</h2>
<h4>Poniższe produkty nie posiadają indeksów</h4>
Produktów do aktualizacji: <b><?php echo count($products); ?></b>
<?php $loop = 0; ?>
<table>
	<colgroup>
		<col width="3%">
	    <col width="75%">
	</colgroup>
	<thead>
		<tr>
			<th>ID</th>
			<th>Nazwa</th>
			<th>Index</th>
			<th>EAN13</th>
		</tr>
	</thead>
</table>
<div style="height: 500px; overflow: auto;">
	<table>
		<colgroup>
			<col width="3%">
	    	<col width="75%">
		</colgroup>
		<tbody>
			<?php 
				foreach ($products as $product ) :
				$ean = preg_replace("/-/","",$product[0]['isbn']);
				$ean = preg_replace("/;/", "", $ean);
				$ean = preg_replace("/ /", "", $ean);
				$index = substr($ean, -8);
				$id_product = $product['p']['id_product'];
			?>
					<tr>
						<td>
							<?php echo $id_product; ?>
						</td>
						<td>
							<?php echo $product['pl']['title']; ?>
						</td>
						<td>
							<?php echo $index; ?>
						</td>
						<td>
							<?php echo $ean; ?>
							<?php if($loop == 0) : ?>
							<button onclick="$.post('/attributes/updateIndexAjax', { reference : <?php echo $index?>, ean13 : <?php echo $ean; ?>, id_product : <?php echo $id_product; ?> })">Test</button>
							<?php endif; ?>
							<span id="index-<?php echo $loop?>" id_product="<?php echo $id_product; ?>" index="<?php echo $index; ?>" ean="<?php echo $ean; ?>"></span>
						</td>
					</tr>
			<?php
				$loop++; 
				endforeach;
			?>
		</tbody>
	</table>
</div>
<div style="float: right; margin: 10px;">
	<?php echo $this->html->image('icons/database_refresh.png', array('title' => 'Aktualizuj', 'style' => 'cursor: pointer' ,'onclick' => 'updateIndexes();')); ?>
</div>
<div id="loader" style="display: none; position: absolute; width: 50px; height: 50px; background: #FFF; border: 1px solid #ccc; top: 0%; right: 3%">
	<?php echo $this->html->image('loading8.gif', array('title' => 'Trwa synchronizacja', 'style' => 'margin: 0px 0px') ); ?>
</div>

<script type="text/javascript">
	function updateIndexes(){
		var count = <?php echo $loop; ?>;
		for(var i = 0; i < count ; i++ ){
			var index = $("#index-"+i).attr('index');
			var ean = $("#index-"+i).attr('ean');
			var product_id = $("#index-"+i).attr('id_product');
			$.ajaxSetup({ async : false });
			$.post('/attributes/updateIndexAjax', { reference : index, ean13 : ean, id_product : product_id }, function(data){
				$("#index-"+i).html(data);
				});
		}
	}
</script>