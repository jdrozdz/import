<h2>Import cen promocyjnych</h2>
<?php $i = 1; ?>
<?php if(count($xml)) : ?>
<div style="background: green; border-radius: 5px; padding: 10px; color: #FFF">
	Plik załadowany poprawnie
</div>
<?php endif; ?>
<table>
<colgroup>
		<col width="2%">
		<col width="20%">
		<col width="20%">
		<col width="20%">
		<col width="20%">
		<col width="20%">
	</colgroup>
	<thead>
		<th>Lp.</th>
		<th align="center">Nazwa produktu<br />ID produktu</th>
		<th>Stara cena</th>
		<th>Nowa cena</th>
		<th>Różnica</th>
		<th>Importuj</th>
	</thead>
	</table>
	<div style="overflow: auto; height: 500px;">
	<table>
	<colgroup>
		<col width="2%">
		<col width="20%">
		<col width="20%">
		<col width="20%">
		<col width="20%">
		<col width="20%">
	</colgroup>
	<tbody>
		<?php 
			foreach ($xml->rows->row as $row) :
			$old_price = preg_replace("/\,/",".", $row['old_price']);
			$price  = preg_replace("/\,/",".", $row['price']);
			if($old_price > 0) :
			$discount = ($old_price - $price);
		?>
		
			<tr id="row-<?php echo $i; ?>">
				<td><?php echo $i; ?></td>
				<td align="center"><?php echo $row['name']."<br />".$row['id']; ?></td>
				<td><?php echo sprintf("%.2f",$row['old_price']); ?></td>
				<td><?php echo sprintf("%.2f",$row['price']); ?></td>
				<td><?php echo sprintf("%.2f",$discount); ?></td>
				<td><?php echo $this->Form->checkbox('import-'.$i, array(
					"hiddenField" => false, 
					"id_product" => $row['id'], 
					'discount' => $discount, 
					'type' => 'amount',
					'value' => $i
				)); $i++?></td>
			</tr>
		<?php
			endif;
		endforeach;
		?>
	</tbody>
</table>
</div>
<div style="position: relative; display:inline-block; right: 1px;">
	<input type="button" value="Zapisz" id="saveButton" /> 
	<input type="button" value="Zaznacz/odznacz wszystko" id="checkerButton"/>
</div>
<script type="text/javascript">
$("#checkerButton").click(function(){
	$('input[type=checkbox]').each(function(){
		if( $(this).is(':checked')){ 
			$(this).removeAttr('checked'); 
		}else{ 
			$(this).attr('checked','checked'); 
		}
	});
});

$("#saveButton").click(function(){
	$('input[type=checkbox]').each(function(){
		if( $(this).is(':checked')){ 
			var el = this;
			$.ajaxSetup({ async: false });
			$.post('/attributes/importAjax', { id_product : $(this).attr('id_product'), discount : $(this).attr('discount') }, function(data){
				var i = $(el).val();
				if(data == "OK"){
					$("#row-"+i).css("background","green").css("color","#FFF");
				}else{
					$("#row-"+i).css("background","red").css("color","#FFF");
				}
				}); 
		}
	});
});
</script>