<?php
class PolishHelper extends AppHelper {
	public function convert($string){
		$string = preg_replace("/Ś/","ś", $string);
		$string = preg_replace("/Ą/","ą", $string);
		$string = preg_replace("/Ć/","ć", $string);
		$string = preg_replace("/Ę/","ę", $string);
		$string = preg_replace("/Ó/","ó", $string);
		$string = preg_replace("/Ń/","ń", $string);
		$string = preg_replace("/Ł/","ł", $string);
		$string = preg_replace("/Ź","ź", $string);
		$string = preg_replace("/Ż/","ż", $string);
		
		return $string;
	}
}
?>