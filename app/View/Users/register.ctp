<h2>Nowy użytkownik</h2>

<?php 
echo $this->Form->create('User');
echo $this->Form->input('username', array('label' => 'Nazwa użytkownika', 'div' => 'input text required', 'required' => true));
echo $this->Form->input('password', array('label' => 'Podaj hasło', 'div' => 'input text required', 'required' => true));
echo $this->Form->input('name', array('label' => 'Imię', 'div' => 'input text required', 'required' => true));
echo $this->Form->input('lastname', array('label' => 'Nazwisko', 'div' => 'input text required', 'required' => true));
echo $this->Form->input('email', array('label' => 'Adres e-mail', 'div' => 'input text required', 'required' => true));
echo $this->Form->end('Rejestruj');
?>