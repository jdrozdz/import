<h2>Import danych magazynowych dla PrestaShop</h2>
<?php echo $this->html->image('new_database.png', array('fullBase' => false, 'title' => 'Dodaj nowy stan magazynowy','url'=>array('controller' => 'index', 'action' => 'sendFile'))); ?>

<table>
	<thead>
		<tr>
			<th>ID</th>
			<th>Plik</th>
			<th>Rozmiar</th>
			<th>Data dodania</th>
			<th>Status</th>
			<th>Opcje</th>
		</tr>
	</thead>
	<tbody>
	<?php
		foreach($files as $file) : 
		$id = $file['Import']['id'];
	?>
		<tr>
			<td>
				<?php echo $file['Import']['id']; ?>
			</td>
			<td>
				<?php echo $file['Import']['filename']; ?>
				<?php if(!empty($file['Import']['id_invertory_doc'])) : ?>
				<br />
				<tt style="background: #339933; padding: 3px; color: #FFF;">Powiązany dokument:
				<?php 
				$invertory_doc = $InvertoryData->findById($file['Import']['id_invertory_doc']);
		
				echo $invertory_doc['InvertoryData']['document_no'];
				echo " z dnia ".$invertory_doc['InvertoryData']['document_date'];
				?>
				</tt>
				<?php endif; ?>
			</td>
			<td>
				<?php echo $this->Number->toReadableSize($file['Import']['filesize']); ?>
			</td>
			<td>
				<?php echo $file['Import']['created_date']; ?>
			</td>
			<td>
				<?php echo $file['Import']['flag']; ?>
			</td>
			<td>
			
				<?php 
				if($file['Import']['flag'] == 'ENABLE'){
					echo $this->html->image('icon_download.gif', array('title' => 'Importuj', 'style' => 'cursor: pointer', 'url' => array('action' => 'importFile', $id)))." | ";
				}
				echo $this->html->image('folder_delete.gif',array('title' => 'Usuń', 'style' => 'cursor: pointer', 'url' => array('action' => 'deleteFile', $id)));
				
				if($file['Import']['flag'] == 'ENABLE'){
				 	echo " | ".$this->html->image('action_stop.gif',array('title' => 'Zablokuj', 'style' => 'cursor: pointer','url' => array('action' => 'lockFile', $id)));
				 }
				 ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</tbody>
</table>