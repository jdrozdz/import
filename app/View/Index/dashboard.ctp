<h1>Moduł synchronizacji danych</h1>
<style type="text/css">
	.option { 
		position: relative; 
		display: inline-block; 
		width: 130px; 
		height: 130px; 
		border: 1px solid #FCFCFC;
		curser: pointer;
		background-image: linear-gradient(bottom, rgb(250,250,250) 27%, rgb(230,230,230) 73%);
		background-image: -o-linear-gradient(bottom, rgb(250,250,250) 27%, rgb(230,230,230) 73%);
		background-image: -moz-linear-gradient(bottom, rgb(250,250,250) 27%, rgb(230,230,230) 73%);
		background-image: -webkit-linear-gradient(bottom, rgb(250,250,250) 27%, rgb(230,230,230) 73%);
		background-image: -ms-linear-gradient(bottom, rgb(250,250,250) 27%, rgb(230,230,230) 73%);
		
		background-image: -webkit-gradient(
			linear,
			left bottom,
			left top,
			color-stop(0.27, rgb(250,250,250)),
			color-stop(0.73, rgb(230,230,230))
		);
	}
	.option:hover {
		background-image: linear-gradient(bottom, rgb(237,237,237) 27%, rgb(214,217,217) 73%);
		background-image: -o-linear-gradient(bottom, rgb(237,237,237) 27%, rgb(214,217,217) 73%);
		background-image: -moz-linear-gradient(bottom, rgb(237,237,237) 27%, rgb(214,217,217) 73%);
		background-image: -webkit-linear-gradient(bottom, rgb(237,237,237) 27%, rgb(214,217,217) 73%);
		background-image: -ms-linear-gradient(bottom, rgb(237,237,237) 27%, rgb(214,217,217) 73%);
		
		background-image: -webkit-gradient(
			linear,
			left bottom,
			left top,
			color-stop(0.27, rgb(237,237,237)),
			color-stop(0.73, rgb(214,217,217))
		);
	}
	.option img { cursor: pointer; }
</style>
<fieldset>
	<legend>Proszę wybrać opcję</legend>
	<div class="option">
		<?php echo $this->html->image('icons/disk_silver_sync.png', array('fulBase' => false, 'title' => 'Synchronizacja danych', 'url' => array('controller' => 'sync'))); ?>
	</div>
	<div class="option">
		<?php echo $this->html->image('icons/document_import.png', array('fulBase' => false, 'title' => 'Import danych', 'url' => array('action' => 'index') )); ?>
	</div>
	<div class="option">
		<?php echo $this->html->image('icons/document_properties.png', array('fulBase' => false, 'title' => 'Import cen', 'url' => array('controller' => 'attributes') )); ?>
	</div>
	<div class="option">
		<?php echo $this->html->image('icons/people.png', array('fulBase' => false, 'title' => 'Dane klientów', 'url' => array('controller' => 'newsletter', 'action' => 'dane') )); ?>
	</div>
	<div class="option">
		<?php echo $this->html->image('icons/program_group.png', array('fullBase' => false, 'title' => 'Pozycjonowanie produktów', 'url' => array('controller' => 'products', 'action' => 'index')))?>
	</div>
	<div class="option">
		<?php echo $this->html->image('icons/dispatch_order_clock.png', array('fullBase' => false, 'width'=>'128','title' => 'Produkty bez indeksów', 'url' => array('controller' => 'attributes', 'action' => 'updateIndexes')))?>
	</div>
</fieldset>