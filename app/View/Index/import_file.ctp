<h2>Import danych</h2>
<h4>Stany magazynowe</h4>
<div style="overflow: auto; width: 100%; display: none; height: 250px;" id="logger"></div>
<?php
echo $this->html->image('back.png', array('fullBase' => false, 'title' => 'Powrót', 'url' => array('controller' => 'index', 'action' => 'index')));
echo "<hr /><br />";

$info = explode("\n", $data);
$import = array();
$not_valid = 0;
$valid = 0;
$loop = 0;
?>

<div style="display: block; position: relative; font-weight: bold;">
	<div style="margin-right: 20px; display: inline-block; position: relative;">Ilość importowanych pozycji: <tt style="font-size: 13px;" id="itemsCount"></tt></div>
	<div style="margin-right: 20px; display: inline-block; position: relative;">Ilość błędnych pozycji lub brakujących: <tt style="font-size: 13px;" id="itemsNotValid"></tt></div>
	<div style="display: inline-block; position: relative;">Ilość znalezionych produktów: <tt style="font-size: 13px;" id="validItems"></tt></div>
</div
>
<div style="position: relative;">
<div style="display: inline-block;">
	<fieldset style="padding: 0; border: 1px groove #333; border-radius: 5px; width: 320px; margin-right: 20px;">
		<legend style="font-family: Thoma; color: #333; margin-left: 10px;">Importuj do</legend>
		<ul style="lsit-style: none;">
			<li style="display: inline-block; margin: 5px 10px; line-height: 23px;"><input style="margin-right: 5px;" checked="checked" type="radio" name="import_type" value="wherehouse" />Magazyn</li>
			<li style="display: inline-block; margin: 5px 10px; line-height: 23px;"><input disabled="disabled" style="margin-right: 5px;" type="radio" name="import_type" value="product" />Ręczne zarządzanie</li>
		</ul>
	</fieldset>
</div><div style="display: inline-block;">
	<fieldset style="padding: 3px 7px; border: 1px groove #333; border-radius: 5px; width: 500px;">
		<legend style="font-family: Thoma; color: #333; margin-left: 10px;">Zamówienie<sup style="color: #990000">*</sup>:</legend>
		<input style="padding: 3px;" value="<?php echo $inv_doc_no; ?>" placeholder="Nr dokumentu obcego" type="text" name="invoice_number" /> 
		<input style="padding: 3px" value="<?php echo $inv_doc_date; ?>" placeholder="Data dokumentu(YYYY-MM-DD)" type="text" name="document_date" />
		<?php  if(empty($inv_doc_no)) : ?>
			<?php echo $this->html->image('icons/add.png', array('fullBase' => false, 'title'=>'Dodaj dokument', 'id' => 'add_invertory_document', 'style' => 'position: absolute; bottom: 18px; cursor: pointer;')); ?>
		<?php endif; ?>
	</fieldset>
	</div></div>
<?php  if(empty($inv_doc_no)) : ?>
	<div style="position: relative; right: 10px; display: none;" id="make_import_form">
		<?php echo $this->html->image('save.png', array('fullBase' => false, 'title' => 'Zapisz' , 'id' => 'make_import')); ?>
		<input type="hidden" name="id_document" />
	</div>
<?php endif; ?>
<table>
	<thead>
		<tr>
			<?php foreach (explode("|",$info[0]) as $header) : ?>
				<th>
					<?php echo preg_replace("/\;/","",$header); ?>
				</th>
			<?php endforeach; ?>
			<th>FL</th>
		</tr>
	</thead>
	<tbody>
		<?php
		unset($info[0]);
		for($i = 1, $j=0; $i < count($info) ; $i++, $j++) :
		$dane = explode("|", $info[$i]);
		$dane[0] = $i;
		$import[$j]['title'] = preg_replace("/\;/","",$dane[3]);
		$import[$j]['isbn'] =  preg_replace("/\;/","",$dane[9]);
		$import[$j]['quantity'] = preg_replace("/\;/","",$dane[5]);
		$import[$j]['was_quant'] = preg_replace("/\;/","",$dane[4]);
		$import[$j]['hurt'] = preg_replace("/,/",".",preg_replace("/\;/","",$dane[7]));
		$import[$j]['hurt'] = preg_replace("/,/",".",preg_replace("/\,/",".",$import[$j]['hurt']));
		
		$res = $importProductMod->find('all',array('conditions' => array(
				'ImportProduct.attr_value'	=> trim($import[$j]['isbn'])
		)));
		$count = count($res);
		
		?>
		<tr <?php echo ($count > 0) ? 'id="row-'.$i.'"' : "" ?>>
		<?php foreach ($dane as $row) : ?>
			<td>
				<?php echo preg_replace("/\;/","",$row); ?>	
			</td>
		<?php endforeach; ?>
			<td>
			<?php
				if($count == 1){
					//pr($res);
					echo $this->html->tag('span','',array(
						'status' => 'ok',
						'id' => "product-".$i,
						'quantity' => (!empty($import[$j]['quantity'])) ? $import[$j]['quantity'] : 0,
						'id_product' => $res[0]['ImportProduct']['id_product'],
						'index'	=> substr(trim($import[$j]['isbn']), -8),
						'was_quant'	=> (!empty($import[$j]['was_quant'])) ? $import[$j]['was_quant'] : 0,
						'ean'	=> trim($import[$j]['isbn']),
						'hurt'	=> (strlen($import[$j]['hurt']) > 1) ? $import[$j]['hurt'] : "0.00"
					));
					echo $this->html->image('/img/status/ok.png', array('title' => $count));
					$valid += 1;
				}elseif($count > 1){
					echo $this->html->image('/img/status/warn.png', array('title' => $count));
					$valid += 1;
				}else{
					echo $this->html->image('/img/status/faild.png', array('title' => $count));
					$not_valid += 1;
				}
				$loop = $i;
			?>
			</td>
		</tr>
		<?php endfor; ?>
	</tbody>
</table>
<?php 
	echo $this->html->scriptBlock(
		'var importedData = '.json_encode($import).';
		$("#itemsCount").html(importedData.length);
		$("#make_import").click(function(){
			var el = '.($loop+1).';//$("span[status=\'ok\']").length
			for(var i=1; i < el; i++){
				var id_product = $("#product-"+i).attr("id_product");
				var quantity = $("#product-"+i).attr("quantity");
				var storage = $("input[name=import_type]");
				var doc_id = $("input[name=id_document]").val();
				var index = $("#product-"+i).attr("index");
				var was_q = $("#product-"+i).attr("was_quant");
				var isbn = $("#product-"+i).attr("ean");
				var hu = $("#product-"+i).attr("hurt");
				
				if(id_product != undefined && quantity != undefined){
					$.ajaxSetup({async:false});
					$.post("/index/importAjax",{ id_product : id_product, quantity : quantity, hurt : hu, was_quantity : was_q, ean : isbn, id_document : doc_id, reference : index },function(data){
							if(data == "OK"){
								$("#row-"+i).css("background","green").css("color","#FFF");
							}else{
								$("#row-"+i).css("background","red").css("color","#FFF");
							}
						});
				}
			}
			alert("Import zakończony");
		});
		$("#itemsNotValid").html("'.$not_valid.'");
		$("#validItems").html("'.$valid.'");
		
		$.ajaxSetup({async:false});
		$("#add_invertory_document").click(function(){
			var doc_no = $("input[name=invoice_number]").val();
			var doc_date = $("input[name=document_date]").val();
			$.post("/index/addInvertoryDocument", { invoice_number : doc_no, document_date : doc_date }, function(data){
				if(parseInt(data) > 0){
					$.post("/index/updateStockDock", { doc_no : data, id_file : '.$id.' });
					$("input[name=id_document]").val(data);
					$("#make_import_form").css("display","block");
				}else{
					alert("Wystąpił błąd, proszę skontaktować się z administratorem");
				}
			});
		});
		
		'
	);
?>