<?php $del = array("usunac", "usunąć"); ?>
<h1>Proces synchronizacji danych</h1>
<tt>Wybrano plik <b><?php echo $filename; ?></b></tt>
<div>
	Postęp synchronizacji: <span id="itemCount"><?php echo count(explode("\n", $data))-1 ?></span>/<span id="position">0</span>
</div>
<div style="overflow: auto; height: 600px; position: relative;">
<table style="position: static; background: #FFF;">
	<thead>
		<?php 
		$data = explode("\n", $data);
		$header = explode(';', $data[0]);
		echo "<tr>";
		echo "<th>Status</th>";
		foreach ($header as $row) :
		?>
		<th>
			<?php 
				echo $row;
			?>
		</th>		
		<?php endforeach; ?>
		</tr>
	</thead>
		<tbody>
			<?php
			$loop = 1;
			unset($data[0]);
			foreach ($data as $row) :		
			?>
			<tr>
				<?php $cells = explode(";", $row);
				if(!empty($cells[9])){
				$col = trim($cells[9]);
				if(in_array($col, $del)){
					$isbn = "delete";	
				}else{
					$isbn = $col;
				}
					$data = '<span id="to-sync-'.$loop.'" isbn="'.$isbn.'" kkname="'.$cells[12].'" quantity="'.$cells[4].'"></span>';
					$loop += 1;
				}else{
					$data = "unknown column";
				}
				echo "<td>".$data."</td>";
				foreach ($cells as $cell) : ?>
					<td><?php echo $cell; ?></td>
				<?php endforeach; ?>
			</tr>
			<?php endforeach; ?>
		</tbody>
	</table>
</div>
<div style="position: absolute; display: inline-block; right: 18px; margin-top: 6px; cursor: pointer;">
	<?php echo $this->html->image("icons/database_refresh.png", array('fullBase' => false, 'title' => 'Synchronizuj', 'id' => 'makeSync')); ?>
</div>
<div id="loader" style="display: none; position: absolute; width: 50px; height: 50px; background: #FFF; border: 1px solid #ccc; top: 0%; right: 3%">
	<?php echo $this->html->image('loading8.gif', array('title' => 'Trwa synchronizacja', 'style' => 'margin: 0px 0px') ); ?>
</div>
<?php 

echo $this->html->scriptBlock('
var xhr;
$("#makeSync").click(function(){
	$("#loader").toggle();
	for(var i = 0; i < '.$loop.'; i++){
		var isbn = $("#to-sync-"+i).attr("isbn");
		var kkname = $("#to-sync-"+i).attr("kkname");
		var quantity = $("#to-sync-"+i).attr("quantity");
		$.ajaxSetup({ async: false });
		xhr = $.post("/sync/makeSync", { name : kkname, isbn : isbn, quantity: quantity }, function(data){
			if(data == "OK"){
				$("#to-sync-"+i).html(\'<span style="padding: 0px 9px; background: #30B527;"></span>\');
				}else{
					if(data == "FAILD"){
						$("#to-sync-"+i).html(\'<span style="padding: 0px 9px; background: #990000;"></span>\');
					}else{
						$("#to-sync-"+i).html(data);
					}
					}
			});
			$("#position").html(i);
	}
	$("#loader").toggle();
});
');

?>