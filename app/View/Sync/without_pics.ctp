<h3>Produkty bez zmienionych obrazków</h3>
<h4>Ilośćproduktów: <?php echo count($items); ?></h4>
<?php
echo $this->form->create('UPCEnable', array('type' => 'get'));
echo $this->form->input('upc', array('value' => ( $upc )));
echo $this->form->end('Filtruj');
?>
<table>
	<thead>
		<th>ID</th><th>ISBN</th><th>Tytuł</th>
	</thead>
	<tbody>
		<?php foreach ($items as $product) : ?>
			<tr>
				<td><?php echo $product['p']['id_product']; ?></td>
				<td><?php echo $product['fvl']['isbn']?>
				<td><?php echo $product['pl']['title']; ?></td>
			</tr>
		<?php endforeach; ?>
	</tbody>
</table>