<style type="text/css">
    label { display: inline-block; margin-right: 15px; }
    #raport-scrolled-table {
        width: 1048px;
        table-layout: fixed;
        margin: 0 auto;
    }
    #raport-scrolled-table tbody {
        display: block;
        height: 500px;
        width: 1080px;
        overflow: auto;
    }
    #raport-scrolled-table tbody td:nth-child(1) { width: 30px; }
    #raport-scrolled-table tbody td:nth-child(2) { width: 700px; }
    #raport-scrolled-table tbody td:nth-child(3) { width: 150px; }
    #raport-scrolled-table tbody td:nth-child(4) { width: 90px; }
    #raport-scrolled-table tbody td:nth-child(5) { width: 90px; }
    
    #raport-scrolled-table thead th:nth-child(1) { width: 30px; }
    #raport-scrolled-table thead th:nth-child(2) { width: 700px; }
    #raport-scrolled-table thead th:nth-child(3) { width: 130px; }
    #raport-scrolled-table thead th:nth-child(4) { width: 90px; }
    #raport-scrolled-table thead th:nth-child(5) { width: 90px; }
    
</style>
<h3>Pozycjonowanie produktów</h3>
<fieldset style="width: 997px; margin: 0 auto;">
    <legend>Filtr produktów</legend>
    <form action="" onsubmit="return false;" class="pure-form">
        <input type="hidden" id="offset" value="0" />
        <input type="hidden" id="page" value="1" />
        
    <label for="limit">
        Ilość pozycji na stronie: 
        <select id="limit">
            <option value="30">30</option>
            <option value="60">60</option>
            <option value="90">90</option>
            <option value="120">120</option>
        </select>
    </label>
    <label for="category">Kategoria:
    <?php
    $list = array();
    echo '<select id="category">';
    echo '<option value="null">-----------[ Wybierz kategorię ]-----------</option>';
    foreach($categories as $cat){
        echo "<option value=\"{$cat["c"]["id_category"]}\">{$cat["cl"]["name"]}</option>";
    }
    echo '</select>';
    ?>
    </label>
        <label for="order">
            Sortuj: <select id="order">
                <option value="asc">Rosnąco</option>
                <option value="desc">Malejąco</option>
            </select>
        </label>
        <label for="col">
            Po kolumnie: <select id="col">
                <option value="p.id_product">Identyfikator (ID)</option>
                <option value="pl.name">Nazwa produktu</option>
                <option value="cp.position">Pozycja</option>
            </select>
        </label>
    <button class="pure-button pure-button-primary" id="send">Filtruj</button>
    </form>
</fieldset>
<!--[if lte IE 9]><div class="old_id_wrapper"><![endif]-->
<table id="raport-scrolled-table">
    <thead>
        <tr>
            <th>#ID</th>
            <th>Nazwa produktu</th>
            <th>EAN</th>
            <th>Pozycja<br />aktualna</th>
            <th>Pozycja</th>
        </tr>
    </thead>
    <tbody id="tbodyRows">
        
    </tbody>
    <tfoot>
        <tr>
            <th colspan="5" align="right"></th>
        </tr>
        <tr>
            <td colspan="5">
                <div id="nav" style="float: right;">
                    <button id="prv" class="pure-button pure-button-primary" style="float: left;margin-left: 10px; display: none;">&lt;</button>
                    <button id="ff" class="pure-button pure-button-primary" style="float: left;display: none;">&gt;</button> 
                </div>
            </td>
        </tr>
    </tfoot>
</table>
<!--[if lte IE 9]></div><![endif]-->
<script type="text/javascript">
    $.ajaxSetup({ async: false });
    
    $("#ff").click(function(){
        $("#page").val(parseInt($("#page").val())+1);
        find();
    });
    $("#prv").click(function(){
        $("#page").val(parseInt($("#page").val())-1);
        find();
    });
    
    function activateNav(obj){
        if(obj.pages > 1){
            console.info("Warunek [0a]");
            if(obj.pages > 1 && obj.page == 1 && obj.pages != obj.page ){
                console.info("Warunek 1");
                $("#ff").css("display","block");
                $("#prv").css("display", "none");
            }else if(obj.page >= 1 && obj.pages > obj.page && obj.pages != obj.page){
                console.info("Warunek 2");
                $("#prv").css("display","block");
            }else if(obj.pages == obj.page){
                console.info("Warunek 3");
                $("#ff").css("display","none");
                $("#prv").css("display","block");
            }
        }else{
                console.info("Warunek [0b]");
                $("#prv").css("display", "none");
                $("#ff").css("display", "none");
            }
    }
    
    function find(){
        var cat = $("#category").val();
        var limit = $("#limit").val();
        var page = $("#page").val();
        var order = $("#order").val();
        var col = $("#col").val();
        if(arguments.length){
            page = 1;
            $("#page").val(1);
        }
        if(cat != "null"){
            $.post("/products/getCategoryProducts", { id_category : cat, order : order, col : col, limit : limit, page : page }, function(data){
                var obj = eval('('+data+')');
                activateNav(obj);
                $("#tbodyRows").empty();
                for(var i=0; i < obj.products.length; i++){
                    var row = "<tr><td>"+obj.products[i].id_product+"</td><td>";
                    row += obj.products[i].name+"</td><td>";
                    row += obj.products[i].ean13+'</td><td style="text-align: center;vertical-align: middle">';
                    row += '<span id="curPos'+obj.products[i].id_product+'" style="background:#66cc66; color: #FFF; padding: 3px 5px;">';
                    row += obj.products[i].position+"</span></td><td>";
                    row += "<input id_prod=\""+obj.products[i].id_product+"\"";
                    row += "type='text' value='";
                    row += obj.products[i].position+"' style=\"width: 30px\" class=\"product-position\" /></td></tr>";
                    $("#tbodyRows").append(row);
                }
                $("#page").val(obj.page);
                $(".product-position").click(function(){
                    var id = $(this).attr("id_prod");
                    $("#curPos"+id).css("background", "#ccff00").css("color","#000");
                }).keyup(function(){
                    var id = $(this).attr("id_prod");
                    var val = parseInt($(this).val());
                    var cat = $("#category").val();
                    
                    if(typeof val === 'number' && (val % 1 == 0)){
                        $.post("/products/updatePosition", { id_product : id, category : cat, position : val }, function(){
                            $("#curPos"+id).css("background", "#cc3300").css("color","#fff");
                        });
                    }else{
                        $('<div title="Błąd!"></div>').html("Podana wartość nie jest liczbą lub nie jest liczba całkowita!").dialog({ modal : true });
                    }
                });
            });
        }else{
            alert("Wybierz kategorię aby rozpocząć pracę!");
        }
    }
    
    $("#send").click(function(){
        find(1);
    });
</script>