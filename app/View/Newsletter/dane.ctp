<h2>Lista subskrybentów</h2>

<table>
	<thead>
		<tr>
			<th>ID</th>
			<th>Imię, Nazwisko<br /><font style="font-size: 10px;">Adres e-mail</font></th>
			<th>Adres</th>
			<th>Miejscowość</th>
			<th>Data zapsania</th>
		</tr>
	</thead>
	<tbody>
		<?php foreach($customers as $customer) : ?>
			<tr>
				<td><?php echo $customer['Newsletter']['id']; ?></td>
				<td>
					<?php echo $customer['Newsletter']['name']." ".$customer['Newsletter']['lastname']; ?><br />
					<font style="font-size: 10px"><a href="mailto: <?php echo $customer['Newsletter']['email']; ?>"><?php echo $customer['Newsletter']['email']; ?></a>
				</td>
				<td><?php echo $customer['Newsletter']['address']; ?></td>
				<td><?php echo $customer['Newsletter']['zipcode']." ".$customer['Newsletter']['city']; ?></td>
				<td><?php echo $customer['Newsletter']['date_added']; ?></td>
			</tr>
		<?php endforeach; ?>
	</tbody>
</table>