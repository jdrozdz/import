<?php
$form = $this->Form;
echo $form->create("customer");
echo $form->input("name", array("label" => 'Imię', 'required' => true, 'div' => 'input text required'));
echo $form->input('lastname', array('label' => 'Nazwisko', 'required' => true, 'div' => 'input text required'));
echo $form->input('address', array('label' => 'Ulica i numer domu', 'required' => true, 'div' => 'input text required'));
echo $form->input('zipcode', array('label' => 'Kod pocztowy', 'required' => true, 'div' => 'input text required'));
echo $form->input('city', array('label' => 'Miejscowość', 'required' => true, 'div' => 'input text required'));
echo $form->input('email', array('label' => 'Adres e-mail', 'required' => true, 'div' => 'input text required'));
echo $form->end('Zapisz');
?>
<form><div class="input text required"><label>Pola wymagane</label></div></form>
<div style="display: block; font-size: 11px; width: 550px; margin: 0 auto; text-align: justify; background: #f0f0f0; box-shadow: 1px 1px 2px rgba(0,0,0,0.3); color: #000; padding: 15px;">
Wpisanie danych do powyższego formularza oznacza wyrażenie zgody na przesyłanie przez firmę Wydawnictwo M Sp. z o. o. 
informacji handlowych drogą elektroniczną oraz pocztową,  zgodnie z zasadami określonymi w obowiązujących przepisach prawa:
Ustawie z dnia 29.08.1997 r. o ochronie danych osobowych (tekst jednolity Dz. U. Nr 101 z 2002 r. poz. 926, z późn. zm.)
Ustawie z dnia 18.07.2002 r. o świadczeniu usług drogą elektroniczną (Dz. U. Nr 144, poz. 1204 z poźn. zm.)rozporządzeniu 
z dnia 29.0.04 r. Ministra Spraw Wewnętrznych i Administracji w sprawie dokumentacji przetwarzania danych osobowych oraz 
warunków technicznych i organizacyjnych, jakim powinny odpowiadać urządzenia i systemy informatyczne, służące do przetwarzania 
danych osobowych (Dz. U. Nr 100, poz.1024).
</div>
<style type="text/css">
    #debug-kit-toolbar { display: none !important; }
    input[type='text'],input[type='email'] { font-size: 12px !important; padding: 5px !important; }
    label { font-size: 12px !important; }
    form div { margin-bottom: 0 !important; }
    body { background: #FFF !important; }
</style>