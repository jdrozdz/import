create view `import_product` AS 
select distinct `p`.`id_product` AS `id_product`,`pl`.`name` AS `name`,`p`.`ean13` AS `ean`,trim(replace(`fvl`.`value`,'-','')) AS `attr_value` 
from `ps_product` `p` 
join `ps_product_lang` `pl` on `p`.`id_product` = `pl`.`id_product`
join `ps_feature_product` `fp` on `fp`.`id_product` = `p`.`id_product`
left join `ps_feature_value_lang` `fvl` on `fvl`.`id_feature_value` = `fp`.`id_feature_value`
where `fvl`.`id_lang` = 1 and `fp`.`id_feature` = 1
